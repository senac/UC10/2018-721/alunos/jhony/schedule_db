package br.com.senac.schedule.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {

    private static final String URL = "jdbc:mysql://localhost:3306/agenda";
    private static final String USER = "root";
    private static final String PASSWORD = "123456";
    private static final String DRIVER = "com.mysql.jdbc.Driver";

    public static Connection getConnection() {

        Connection connection = null;

        try {

            Class.forName(DRIVER);

            connection = (Connection) DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Conectado ao banco !");

        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            
            System.out.println("Erro ao conectar com o banco");
            ex.printStackTrace();
        }
        return connection;
    }
    
    public static void main(String[] args) {
        Conexao.getConnection();
    }
}

