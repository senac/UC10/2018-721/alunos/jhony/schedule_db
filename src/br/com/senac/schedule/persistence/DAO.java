package br.com.senac.schedule.persistence;

import br.com.senac.schedule.model.Pessoa;
import java.util.List;

public interface DAO<T> {

    void inserir(T objeto);

    void atualizar(T objeto);

    void delete(T objeto);
    
    List<T> listarTodos();
}
