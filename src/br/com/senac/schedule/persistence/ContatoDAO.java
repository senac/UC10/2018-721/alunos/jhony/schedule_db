package br.com.senac.schedule.persistence;

import br.com.senac.schedule.model.Pessoa;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ContatoDAO implements DAO<Pessoa> {

    @Override
    public void inserir(Pessoa pessoa) {

        Connection connection = null;

        try {
            String query = "insert into agenda (NOME , TELEFONE) values (?, ?)";
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, pessoa.getNome());
            ps.setString(2, pessoa.getTelefone());
            int linhas = ps.executeUpdate();
            if (linhas > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                int id = rs.getInt(1);
                pessoa.setId(id);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void atualizar(Pessoa pessoa) {
        Connection connection = null;
        try {
            String query = "UPDATE AGENDA SET NOME = ? , TELEFONE = ? WHERE ID = ?";
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, pessoa.getNome());
            ps.setString(2, pessoa.getTelefone());
            ps.setInt(3, pessoa.getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
   
    @Override
    public void delete(Pessoa pessoa) {
        Connection connection = null;
        try {
            String query = "DELETE FROM AGENDA WHERE ID = ?";
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, pessoa.getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public List<Pessoa> listarTodos() {
        Connection connection = null;
        List<Pessoa> lista = new ArrayList<>();

        try {

            connection = Conexao.getConnection();
            String query = "SELECT * FROM AGENDA";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                int id = rs.getInt("id");
                String nome = rs.getString("nome");
                String telefone = rs.getString("telefone");
                Pessoa pessoa = new Pessoa(id, nome, telefone);
                lista.add(pessoa);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return lista;
    }

    public List<Pessoa> listarPorNome(String nomeBuscado) {
        Connection connection = null;
        List<Pessoa> lista = new ArrayList<>();

        try {

            connection = Conexao.getConnection();
            String query = "SELECT * FROM AGENDA WHERE NOME LIKE ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, nomeBuscado + "%");

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String nome = rs.getString("nome");
                String telefone = rs.getString("telefone");
                Pessoa pessoa = new Pessoa(id, nome, telefone);
                lista.add(pessoa);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return lista;
    }

    public static void main(String[] args) {

        ContatoDAO dao = new ContatoDAO();
        Pessoa p = new Pessoa("dasdad", "dsdasdasd");
        dao.inserir(p);

    }

    public int getQuantidadeCotatos() {

        Connection connection = null;
        int quantidade = 0;

        try {

            connection = Conexao.getConnection();
            String query = "select count(1) from agenda";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            rs.first();
            quantidade = rs.getInt(1);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return quantidade;

    }

}
