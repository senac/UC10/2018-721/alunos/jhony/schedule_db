
package br.com.senac.schedule.view;

import javax.swing.JOptionPane;
import javax.swing.JFrame;

public abstract class JFrameModelo extends JFrame{

    public JFrameModelo() {
        this.setVisible(true);
    }
    
    
    
    public void showMessageInformacao (String mensagem){
        JOptionPane.showMessageDialog(this, mensagem, "Informação", 1);
    }
    
    public void showMessageErro (String mensagem){
        JOptionPane.showMessageDialog(this, mensagem, "Erro", 0);
    }
    
    public void showMessageAlerta (String mensagem){
        JOptionPane.showMessageDialog(this, mensagem, "Alerta", 2);
    }
    
    
    
}
